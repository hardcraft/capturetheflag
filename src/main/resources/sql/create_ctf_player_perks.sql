CREATE TABLE IF NOT EXISTS `ctf_player_perks` (
  `player_id` INT(11) NOT NULL,
  `tnt_power` DECIMAL(19,4) NOT NULL DEFAULT 20,
  `tnt_amount` INT(11) NOT NULL DEFAULT 1,
  `tnt_protection` DECIMAL(19,4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`player_id`),
  UNIQUE INDEX `player_id_UNIQUE` (`player_id` ASC),
  CONSTRAINT `fk_ctf_player_perks_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;