package com.gmail.val59000mc.ctf.events;

import com.gmail.val59000mc.ctf.flags.Flag;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;

public class CTFFlagReturnedToBaseEvent extends HCEvent {

    private Flag flag;

    public CTFFlagReturnedToBaseEvent(HCGameAPI api, Flag flag) {
        super(api);
        this.flag = flag;
    }

    public Flag getFlag() {
        return flag;
    }

}
