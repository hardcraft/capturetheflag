package com.gmail.val59000mc.ctf.listeners;

import com.gmail.val59000mc.ctf.players.CTFPlayer;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBPersistedSessionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.spigotutils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;

public class CTFMySQLListener extends HCListener {

    // Queries
    private String createCTFPlayerSQL;
    private String createCTFPlayerStatsSQL;
    private String createCTFPlayerPerksSQL;
    private String insertCTFPlayerSQL;
    private String insertCTFPlayerStatsSQL;
    private String insertCTFPlayerPerksSQL;
    private String updateCTFPlayerGlobalStatsSQL;
    private String updateCTFPlayerStatsSQL;
    private String selectCTFPlayerPerksSQL;

    /**
     * Load sql queries from resources files
     */
    public void readQueries() {
        HCMySQLAPI sql = getMySQLAPI();
        if (sql.isEnabled()) {
            createCTFPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/create_ctf_player.sql");
            createCTFPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/create_ctf_player_stats.sql");
            createCTFPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/create_ctf_player_perks.sql");
            insertCTFPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/insert_ctf_player.sql");
            insertCTFPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/insert_ctf_player_stats.sql");
            insertCTFPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/insert_ctf_player_perks.sql");
            updateCTFPlayerGlobalStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_ctf_player_global_stats.sql");
            updateCTFPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_ctf_player_stats.sql");
            selectCTFPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/select_ctf_player_perks.sql");
        }
    }

    /**
     * Insert game if not exists
     *
     * @param e
     */
    @EventHandler
    public void onGameFinishedLoading(HCAfterLoadEvent e) {
        HCMySQLAPI sql = getMySQLAPI();

        readQueries();

        if (sql.isEnabled()) {

            Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
                @Override
                public void run() {

                    try {

                        sql.execute(sql.prepareStatement(createCTFPlayerSQL));

                        sql.execute(sql.prepareStatement(createCTFPlayerStatsSQL));

                        sql.execute(sql.prepareStatement(createCTFPlayerPerksSQL));

                    } catch (SQLException e) {
                        Logger.severe("Couldnt create tables for CTF stats or perks");
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    /**
     * Add new player if not exists when joining
     * Update name (because it may change)
     * Update current played game
     *
     * @param e
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(HCPlayerDBInsertedEvent e) {
        HCMySQLAPI sql = getMySQLAPI();

        if (sql.isEnabled()) {

            Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
                @Override
                public void run() {

                    try {
                        String id = String.valueOf(e.getHcPlayer().getId());

                        // Insert ctf player if not exists
                        sql.execute(sql.prepareStatement(insertCTFPlayerSQL, id));

                        // Insert ctf player stats if not exists
                        sql.execute(sql.prepareStatement(insertCTFPlayerStatsSQL, id, sql.getMonth(), sql.getYear()));

                        sql.execute(sql.prepareStatement(insertCTFPlayerPerksSQL, id));

                        CachedRowSet perks = sql.query(sql.prepareStatement(selectCTFPlayerPerksSQL, id));
                        perks.first();
                        CTFPlayer ctfPlayer = (CTFPlayer) e.getHcPlayer();
                        ctfPlayer.setTntAmount(perks.getInt("tnt_amount"));
                        ctfPlayer.setTntProtection(perks.getDouble("tnt_protection"));
                        ctfPlayer.setTntPower(perks.getDouble("tnt_power"));

                    } catch (SQLException ex) {
                        Log.severe("Couldn't find perks for player " + e.getHcPlayer().getName());
                        ex.printStackTrace();
                    }
                }
            });
        }
    }


    /**
     * Save all players global data when game ends
     *
     * @param e
     */
    @EventHandler
    public void onGameEnds(HCPlayerDBPersistedSessionEvent e) {
        HCMySQLAPI sql = getMySQLAPI();

        if (sql.isEnabled()) {

            CTFPlayer ctfPlayer = (CTFPlayer) e.getHcPlayer();

            // Launch one async task to save all data
            Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
                @Override
                public void run() {

                    try {
                        // Update ctf player global stats
                        sql.execute(sql.prepareStatement(updateCTFPlayerGlobalStatsSQL,
                            String.valueOf(ctfPlayer.getFlagsTaken()),
                            String.valueOf(ctfPlayer.getFlagsSaved()),
                            String.valueOf(ctfPlayer.getTimePlayed()),
                            String.valueOf(ctfPlayer.getKills()),
                            String.valueOf(ctfPlayer.getDeaths()),
                            String.valueOf(ctfPlayer.getMoney()),
                            String.valueOf(ctfPlayer.getWins()),
                            String.valueOf(ctfPlayer.getId())
                        ));


                        // Update ctf player stats
                        sql.execute(sql.prepareStatement(updateCTFPlayerStatsSQL,
                            String.valueOf(ctfPlayer.getFlagsTaken()),
                            String.valueOf(ctfPlayer.getFlagsSaved()),
                            String.valueOf(ctfPlayer.getTimePlayed()),
                            String.valueOf(ctfPlayer.getKills()),
                            String.valueOf(ctfPlayer.getDeaths()),
                            String.valueOf(ctfPlayer.getMoney()),
                            String.valueOf(ctfPlayer.getWins()),
                            String.valueOf(ctfPlayer.getId()),
                            sql.getMonth(),
                            sql.getYear()
                        ));

                    } catch (SQLException e) {
                        Logger.severe("Couldnt update CTF player stats for player=" + ctfPlayer.getName() + " uuid=" + ctfPlayer.getUuid());
                        e.printStackTrace();
                    }

                }
            });
        }

    }
}
