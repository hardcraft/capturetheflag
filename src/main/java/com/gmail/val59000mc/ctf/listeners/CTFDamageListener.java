package com.gmail.val59000mc.ctf.listeners;

import com.gmail.val59000mc.ctf.players.CTFPlayer;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import net.minecraft.server.v1_14_R1.EntityPlayer;
import net.minecraft.server.v1_14_R1.EntityTNTPrimed;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftTNTPrimed;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.lang.reflect.Field;

public class CTFDamageListener extends HCListener {

    @EventHandler(priority = EventPriority.LOW)
    public void onBlockBreak(EntityDamageByEntityEvent e) {
        // Lowering TNT damage
        if (e.getEntity() instanceof Player && e.getDamager() instanceof TNTPrimed) {

            CTFPlayer hcDamaged = (CTFPlayer) getPmApi().getHCPlayer((Player) e.getEntity());

            if (hcDamaged != null) {

                double tntProtection = hcDamaged.getTntProtection();


                // Getting damager tnt power
                double tntPower = 1; // default power is max tnt power 100%
                TNTPrimed tnt = (TNTPrimed) e.getDamager();
                EntityTNTPrimed nmsTNT = (((CraftTNTPrimed) tnt).getHandle());
                try {
                    Field source = EntityTNTPrimed.class.getDeclaredField("source");
                    source.setAccessible(true);
                    if (source.get(nmsTNT) != null && source.get(nmsTNT) instanceof EntityPlayer) {
                        EntityPlayer entityPlayer = (EntityPlayer) source.get(nmsTNT);
                        CTFPlayer hcDamager = (CTFPlayer) getPmApi().getHCPlayer(entityPlayer.getUniqueID());
                        if (hcDamager != null) {
                            tntPower = hcDamager.getTntPower();
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                double finalDamage = e.getDamage();
                Log.debug(hcDamaged.getName() + " initial damage=" + finalDamage);
                finalDamage = finalDamage * 0.01 * tntPower; // setting damager power
                Log.debug(hcDamaged.getName() + " affected tntPower=" + tntPower);
                Log.debug(hcDamaged.getName() + " after power damage=" + finalDamage);
                finalDamage -= finalDamage * 0.01 * tntProtection; // setting damaged defense
                Log.debug(hcDamaged.getName() + " affected tntProtection=" + tntProtection);
                Log.debug(hcDamaged.getName() + " after protection damage=" + finalDamage);
                e.setDamage(finalDamage);

            }

        }
    }

}
