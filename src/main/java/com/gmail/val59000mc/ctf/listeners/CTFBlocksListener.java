package com.gmail.val59000mc.ctf.listeners;

import com.gmail.val59000mc.ctf.common.Constants;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.items.ItemUtils;
import com.google.common.collect.Sets;
import net.minecraft.server.v1_14_R1.EntityLiving;
import net.minecraft.server.v1_14_R1.EntityTNTPrimed;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftTNTPrimed;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.lang.reflect.Field;
import java.util.Set;

public class CTFBlocksListener extends HCListener {

    private LocationBounds allowedBuildZone;

    private boolean cancelWaterFlow = true;
    private Set<Material> forbiddenFlow = Sets.newHashSet(Material.WATER, Material.LAVA);


    @EventHandler
    public void afterLoad(HCAfterLoadEvent e) {

        World ctf = getApi().getWorldConfig().getWorld();

        Location buildZoneMin = Parser.parseLocation(ctf, getConfig().getString("allow-build.min"));
        Location buildZoneMax = Parser.parseLocation(ctf, getConfig().getString("allow-build.max"));

        this.allowedBuildZone = new LocationBounds(buildZoneMin, buildZoneMax);

        this.cancelWaterFlow = getConfig().getBoolean("cancel-water-flow", Constants.DEFAULT_CANCEL_WATER_FLOW);
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent e) {
        if (!ItemUtils.isWool(e.getItemDrop().getItemStack().getType())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onWalkOnPressurePlate(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (!player.isDead() && e.getAction() == Action.PHYSICAL && e.hasBlock()) {
            Block plate = e.getClickedBlock();
            if (plate != null && plate.getType().equals(Material.HEAVY_WEIGHTED_PRESSURE_PLATE)) {
                Block beneath = plate.getRelative(0, -1, 0);
                if (ItemUtils.isColoredTerracotta(beneath.getType())) {
                    HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
                    if (hcPlayer != null && hcPlayer.is(PlayerState.PLAYING)) {
                        getSoundApi().play(Sound.ENTITY_FIREWORK_ROCKET_TWINKLE, player.getLocation(), 1.2f, 2);

                        Location playerLoc = player.getLocation().clone();
                        Vector velocity = playerLoc.getDirection().normalize().multiply(0.15).setY(1);
                        Bukkit.getScheduler().runTaskLater(getPlugin(), () -> {
                            player.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, player.getLocation(), 25, 1, 1, 1, 1);
                            player.setVelocity(velocity);
                        }, 1);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        // Block breaking other things than wool
        if (!ItemUtils.isWool(e.getBlock().getType())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onUser(PlayerInteractEvent e) {
        if (!getApi().is(GameState.PLAYING)
            && e.getClickedBlock() != null
            && ItemUtils.isWoodenPressurePlate(e.getClickedBlock().getType())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        Block block = e.getBlock();
        if (block.getType().equals(Material.TNT)) {

            Player player = e.getPlayer();
            Location tntLocation = block.getLocation().clone().add(0.5, 0, 0.5);

            Block tntBlock = tntLocation.getWorld().getBlockAt(tntLocation);
            tntBlock.setType(Material.AIR);

            // Spawn a tnt
            TNTPrimed tnt = (TNTPrimed) player.getWorld().spawnEntity(tntLocation, EntityType.PRIMED_TNT);
            tnt.setFuseTicks(30);
            tnt.setIsIncendiary(false);

            // Change via NMS the source of the TNT by the player
            EntityLiving nmsEntityLiving = (((CraftLivingEntity) player).getHandle());
            EntityTNTPrimed nmsTNT = (((CraftTNTPrimed) tnt).getHandle());
            try {
                Field source = EntityTNTPrimed.class.getDeclaredField("source");
                source.setAccessible(true);
                source.set(nmsTNT, nmsEntityLiving);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            Inventories.remove(player.getInventory(), new ItemStack(Material.TNT), new ItemCompareOption.Builder().withLore(true).build());

            e.setCancelled(true);


        } else if (ItemUtils.isWool(block.getType())) {
            if (!allowedBuildZone.contains(block.getLocation())) {
                e.setCancelled(true);
            }
        } else {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockExplode(EntityExplodeEvent event) {
        event.blockList().removeIf(block -> !ItemUtils.isWool(block.getType()));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockExplode(BlockExplodeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockFromTo(BlockFromToEvent e) {
        if (cancelWaterFlow) {
            Material mat = e.getBlock().getType();
            if (forbiddenFlow.contains(mat)) {
                e.setCancelled(true);
            }
        }
    }
}
