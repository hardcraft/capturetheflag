package com.gmail.val59000mc.ctf.listeners;

import com.gmail.val59000mc.ctf.callbacks.CTFCallbacks;
import com.gmail.val59000mc.ctf.flags.FlagManager;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import org.bukkit.event.EventHandler;

public class CTFGamePlayListener extends HCListener {

    @EventHandler
    public void afterLoad(HCAfterLoadEvent e) {
        FlagManager fm = ((CTFCallbacks) getCallbacksApi()).getFm();
        fm.createFlags();

        getApi().registerListener(new CTFFlagListener(fm));
    }

}
