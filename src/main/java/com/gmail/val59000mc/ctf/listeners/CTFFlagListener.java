package com.gmail.val59000mc.ctf.listeners;

import com.gmail.val59000mc.ctf.flags.Flag;
import com.gmail.val59000mc.ctf.flags.FlagManager;
import com.gmail.val59000mc.ctf.flags.FlagState;
import com.gmail.val59000mc.ctf.players.CTFPlayer;
import com.gmail.val59000mc.ctf.players.CTFTeam;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledByPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.spigotutils.items.ItemUtils;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

public class CTFFlagListener extends HCListener {

    private FlagManager fm;

    public CTFFlagListener(FlagManager fm) {
        this.fm = fm;
    }

    @EventHandler
    public void onPlayerDisconnect(HCPlayerQuitEvent e) {
        CTFPlayer ctfPlayer = (CTFPlayer) e.getHcPlayer();
        if (ctfPlayer.isCarryingFlag()) {
            fm.dropFlag(ctfPlayer);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (!e.getPlayer().isDead()) {
            Flag flag = fm.getFlagOn(e.getTo()); // Get flag object on base station (may have been taken)
            if (flag != null) { // if player is on base station then flag != null
                CTFPlayer ctfPlayer = (CTFPlayer) getPmApi().getHCPlayer(e.getPlayer());
                if (ctfPlayer != null && ctfPlayer.is(PlayerState.PLAYING) && ctfPlayer.hasTeam()) {
                    if (ctfPlayer.isCarryingFlag() && flag.getTeam().equals(ctfPlayer.getTeam())) { // player has brought back the ennemy black
                        fm.scoreFlag(ctfPlayer);
                    } else if (!ctfPlayer.isCarryingFlag() && flag.is(FlagState.BASE) && flag.getTeam().equals(((CTFTeam) ctfPlayer.getTeam()).getEnemyTeam())) { // player is stealing the ennemy flag
                        fm.takeFlag(ctfPlayer, flag);
                    }
                }
            }
        }
    }

    // Deny moving banner item in inventory
    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getWhoClicked() instanceof Player) {

            ItemStack item = e.getCurrentItem();
            if (item != null && ItemUtils.isBanner(item.getType())) {
                HCPlayer hcPlayer = getPmApi().getHCPlayer((Player) e.getWhoClicked());
                if (hcPlayer != null) {
                    getStringsApi().get("ctf.cant-move-flag-item").sendChatP(hcPlayer);
                    getSoundApi().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 1f, 2f);
                    e.setCancelled(true);
                }
            }

        }
    }

    @EventHandler
    public void onPlayerKilled(HCPlayerKilledByPlayerEvent e) {
        CTFPlayer ctfKilled = (CTFPlayer) e.getHCKilled();
        CTFPlayer ctfKiller = (CTFPlayer) e.getHcKiller();

        if (ctfKilled.isCarryingFlag())
            fm.dropFlagBy(ctfKilled, ctfKiller);

    }


    @EventHandler
    public void onPlayerKilledBy(HCPlayerKilledEvent e) {
        CTFPlayer ctfPlayer = (CTFPlayer) e.getHcPlayer();

        if (ctfPlayer.isCarryingFlag())
            fm.dropFlag(ctfPlayer);

    }

}
