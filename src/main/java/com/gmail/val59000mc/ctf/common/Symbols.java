package com.gmail.val59000mc.ctf.common;

public class Symbols {
    public static final String square = "\u25A1";
    public static final String filledSquare = "\u25A3";
    public static final String arrow = "\u25B7";
    public static final String filledArrow = "\u25B6";
    public static final String bigSquare = "\u2587";
    public static final String flag = "\u2617";

}
