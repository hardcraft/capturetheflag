package com.gmail.val59000mc.ctf.common;

import com.gmail.val59000mc.ctf.players.CTFPlayer;
import com.gmail.val59000mc.ctf.players.CTFTeam;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.spigotutils.items.*;
import com.google.common.collect.Lists;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

import java.util.List;

public class CTFItems {


    public static Color getBukkitColor(CTFTeam team) {
        switch (team.getColor()) {
            case RED:
                return Color.fromRGB(255, 0, 0);
            default:
            case BLUE:
                return Color.fromRGB(0, 0, 255);
        }
    }

    public static ItemStack getBanner(CTFTeam team) {
        DyeColor dyeColor = DyeColor.valueOf(team.getColor().name());

        return new ItemBuilder(ItemUtils.toBannerMaterial(team.getColor()))
            .buildMeta(BannerMetaBuilder.class)
            .withPattern(new Pattern(DyeColor.BLACK, PatternType.GRADIENT_UP))
            .withPattern(new Pattern(dyeColor, PatternType.TRIANGLES_TOP))
            .withPattern(new Pattern(dyeColor, PatternType.TRIANGLES_BOTTOM))
            .withPattern(new Pattern(dyeColor, PatternType.CURLY_BORDER))
            .withPattern(new Pattern(dyeColor, PatternType.BORDER))
            .withPattern(new Pattern(dyeColor, PatternType.STRIPE_SMALL))
            .item()
            .build();
    }

    public static ItemStack getHelmet(CTFTeam team) {

        Color color = getBukkitColor(team);

        return new ItemBuilder(Material.LEATHER_HELMET)
            .buildMeta(LeatherArmorMetaBuilder.class)
            .withColor(color)
            .withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false)
            .item()
            .build();

    }


    public static List<ItemStack> getArmor(HCPlayer hcPlayer) {

        Color color = getBukkitColor((CTFTeam) hcPlayer.getTeam());

        return Lists.newArrayList(
            new ItemBuilder(Material.LEATHER_HELMET)
                .buildMeta(LeatherArmorMetaBuilder.class)
                .withColor(color)
                .withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false)
                .item()
                .build(),
            new ItemBuilder(Material.LEATHER_CHESTPLATE)
                .buildMeta(LeatherArmorMetaBuilder.class)
                .withColor(color)
                .withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false)
                .item()
                .build(),
            new ItemBuilder(Material.LEATHER_LEGGINGS)
                .buildMeta(LeatherArmorMetaBuilder.class)
                .withColor(color)
                .withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false)
                .item()
                .build(),
            new ItemBuilder(Material.LEATHER_BOOTS)
                .buildMeta(LeatherArmorMetaBuilder.class)
                .withColor(color)
                .withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true)
                .withEnchant(Enchantment.PROTECTION_FALL, 2, true)
                .item()
                .build()
        );
    }

    public static List<ItemStack> getItems(HCPlayer hcPlayer) {

        return Lists.newArrayList(
            new ItemBuilder(Material.STONE_SWORD)
                .buildMeta()
                .withUnbreakable(true)
                .item()
                .build(),
            new ItemBuilder(Material.BOW)
                .buildMeta()
                .withUnbreakable(true)
                .item()
                .build(),
            new ItemStack(Material.COOKED_BEEF, 16),
            new ItemStack(Material.TNT, ((CTFPlayer) hcPlayer).getTntAmount()),
            makePotion(PotionType.SPEED), // speed II
            makePotion(PotionType.INSTANT_DAMAGE), // damage II
            new ItemStack(Material.SNOWBALL, 16),
            new ItemBuilder(Material.SHEARS)
                .buildMeta()
                .withEnchant(Enchantment.DIG_SPEED, 3, false)
                .withUnbreakable(true)
                .item()
                .build(),
            new ItemStack(ItemUtils.toWoolMaterial(hcPlayer.getColor()), 64),
            new ItemStack(Material.ARROW, 64)
        );
    }

    private static ItemStack makePotion(PotionType effect) {
        ItemBuilder item = new ItemBuilder(Material.SPLASH_POTION);
        PotionMetaBuilder potion = new PotionMetaBuilder(item);
        potion.withMainEffect(effect, false, true);
        item.withMeta(potion);
        return item.build();
    }
}
