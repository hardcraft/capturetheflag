package com.gmail.val59000mc.ctf.callbacks;

import com.gmail.val59000mc.ctf.common.CTFItems;
import com.gmail.val59000mc.ctf.common.Symbols;
import com.gmail.val59000mc.ctf.flags.Flag;
import com.gmail.val59000mc.ctf.flags.FlagManager;
import com.gmail.val59000mc.ctf.flags.FlagState;
import com.gmail.val59000mc.ctf.players.CTFPlayer;
import com.gmail.val59000mc.ctf.players.CTFTeam;
import com.gmail.val59000mc.hcgameslib.api.impl.DefaultPluginCallbacks;
import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.hcgameslib.worlds.WorldConfig;
import com.gmail.val59000mc.hcgameslib.worlds.WorldManager;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.BungeeTpAction;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemModifier;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Blocks;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.Texts;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.spigotutils.items.ItemUtils;
import com.google.common.collect.Lists;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.ArrayList;
import java.util.List;

public class CTFCallbacks extends DefaultPluginCallbacks {

    private FlagManager fm;

    @Override
    public HCPlayer newHCPlayer(Player player) {
        return new CTFPlayer(player);
    }

    @Override
    public HCPlayer newHCPlayer(HCPlayer hcPlayer) {
        return new CTFPlayer(hcPlayer);
    }

    public FlagManager getFm() {
        return fm;
    }

    @Override
    public List<HCTeam> createTeams() {

        World world = getApi().getWorldConfig().getWorld();
        Location defLoc = getApi().getWorldConfig().getCenter();

        // Parse red locations
        CTFTeam redTeam;
        List<String> redLocationStr = getConfig().getStringList("teams.red.spawnpoints");
        if (redLocationStr == null) {
            redTeam = new CTFTeam("Rouge", ChatColor.RED, Lists.newArrayList(defLoc));
        } else {
            List<Location> redLoc = new ArrayList<Location>();
            for (String str : redLocationStr) {
                redLoc.add(Parser.parseLocation(world, str));
            }
            redTeam = new CTFTeam("Rouge", ChatColor.RED, redLoc);
        }


        // Parse blue locations
        CTFTeam blueTeam;
        List<String> blueLocationStr = getConfig().getStringList("teams.blue.spawnpoints");
        if (blueLocationStr == null) {
            blueTeam = new CTFTeam("Bleu", ChatColor.BLUE, Lists.newArrayList(defLoc));
        } else {
            List<Location> blueLoc = new ArrayList<Location>();
            for (String str : blueLocationStr) {
                blueLoc.add(Parser.parseLocation(world, str));
            }
            blueTeam = new CTFTeam("Bleu", ChatColor.BLUE, blueLoc);
        }

        redTeam.setEnemyTeam(blueTeam);
        blueTeam.setEnemyTeam(redTeam);

        return Lists.newArrayList(redTeam, blueTeam);
    }

    @Override
    public WorldConfig configureWorld(WorldManager wm) {

        WorldConfig worldCfg = super.configureWorld(wm);

        this.fm = new FlagManager(getApi());

        return worldCfg;
    }


    /**
     * Called before a player is given his stuff
     * Allow to change the stuff at every respawn
     * Set stuff to null to not give anything
     * Or set inv content to null or armor content to null
     */
    @Override
    public void assignStuffToPlayer(HCPlayer hcPlayer) {

        if (hcPlayer.getStuff() == null && hcPlayer.hasTeam() && hcPlayer.isOnline()) {
            hcPlayer.setStuff(
                new Stuff.Builder(hcPlayer.getName())
                    .addArmorItems(CTFItems.getArmor(hcPlayer))
                    .addInventoryItems(CTFItems.getItems(hcPlayer))
                    .build()
            );
        }
    }

    @Override
    public Location getNextRespawnLocation(HCPlayer hcPlayer) {
        Location spawnpoint;
        if (hcPlayer.hasTeam())
            spawnpoint = ((CTFTeam) hcPlayer.getTeam()).getRandomSpawnpoint();
        else
            spawnpoint = hcPlayer.getSpawnPoint();

        if (spawnpoint != null) {
            for (Block block : Blocks.findNearbyBlocks(spawnpoint, 3, ItemUtils.WOOL_MATERIALS)) {
                block.setType(Material.AIR);
            }
        }

        return spawnpoint;
    }

    @Override
    public void startPlayer(HCPlayer hcPlayer) {
        super.startPlayer(hcPlayer);
        hcPlayer.getPlayer().setGameMode(GameMode.SURVIVAL);
    }

    @Override
    public void revivePlayer(HCPlayer hcPlayer) {
        super.revivePlayer(hcPlayer);
        hcPlayer.getPlayer().setGameMode(GameMode.SURVIVAL);
    }

    @Override
    public void handleKillEvent(PlayerDeathEvent event, HCPlayer hcKilled, HCPlayer hcKiller) {
        super.handleKillEvent(event, hcKilled, hcKiller); // auto respawn
        event.getDrops().clear();
    }


    @Override
    public void handleDeathEvent(PlayerDeathEvent event, HCPlayer hcKilled) {
        super.handleDeathEvent(event, hcKilled); // auto respawn
        event.getDrops().clear();
    }

    /**
     * Update the scoreboard of a playing HCPlayer
     * Must return the lines of the scoreboard
     *
     * @return lines
     */
    @Override
    public List<String> updatePlayingScoreboard(HCPlayer hcPlayer) {

        List<String> content = new ArrayList<String>();

        // All teams flags
        content.add(getStringsApi().get("messages.scoreboard.scores").toString());
        for (HCTeam team : getPmApi().getTeams()) {
            content.add(" " + team.getColor() + team.getName() + " " + getTeamFlagScoreDisplay((CTFTeam) team));
        }

        // Remaining time (if exists)
        if (getApi().is(GameState.PLAYING) && getApi().isCountdownEndOfGameEnabled()) {
            int remainingTime = getApi().getRemainingTimeBeforeEnd();
            content.add(
                getStringsApi().get("messages.scoreboard.remaining-time-before-end").toString() +
                    " " +
                    ChatColor.GREEN + Time.getFormattedTime(remainingTime)
            );
        }

        // Kills
        content.add(getStringsApi().get("messages.scoreboard.kills-deaths").toString());
        content.add(" " + ChatColor.GREEN + "" + hcPlayer.getKills() + ChatColor.WHITE + "/" + ChatColor.GREEN + hcPlayer.getDeaths());

        // Coins
        content.add(getStringsApi().get("messages.scoreboard.coins").toString());
        content.add(" " + ChatColor.GREEN + "" + hcPlayer.getMoney());

        // Flags
        if (hcPlayer.hasTeam()) {
            CTFTeam ctfTeam = (CTFTeam) hcPlayer.getTeam();
            content.add(getStringsApi().get("ctf.scoreboard.flag.ally").toString() + " " + ctfTeam.getColor() + "§l" + Symbols.filledArrow);
            content.addAll(getFlagProgress(ctfTeam.getFlag()));
            content.add(" ");

            content.add(getStringsApi().get("ctf.scoreboard.flag.enemy").toString() + " " + ctfTeam.getEnemyTeam().getColor() + "§l" + Symbols.filledArrow);
            content.addAll(getFlagProgress(ctfTeam.getEnemyTeam().getFlag()));
        } else {
            content.add(getStringsApi().get("ctf.scoreboard.flags").toString());
            for (HCTeam team : getPmApi().getTeams()) {
                CTFTeam ctfTeam = (CTFTeam) team;
                content.addAll(getFlagProgressForSpec(ctfTeam.getFlag()));
                content.add(" ");
            }
        }

        return content;
    }

    private String getTeamFlagScoreDisplay(CTFTeam team) {
        String score = "";
        int limit = getConfig().getInt("flags-limit", 3);
        for (int i = 1; i <= limit; i++) {
            if (team.getFlagsTaken() >= i) {
                score += team.getColor() + Symbols.flag;
            } else {
                score += ChatColor.GRAY + Symbols.flag;
            }
        }
        return score;
    }

    private List<String> getFlagProgress(Flag flag) {
        List<String> content = new ArrayList<String>();

        if (flag.is(FlagState.BASE)) {
            content.add(" §a" + getStringsApi().get("ctf.scoreboard.flag.secured").toString());
        } else {
            content.add(" §6" + getStringsApi().get("ctf.scoreboard.flag.stolen-by").toString() + "§l" + flag.getHeldBy().getColor() + flag.getHeldBy().getName());

            // Progress bar
            int progress = fm.getProgress(flag);
            String progressBar = " ";
            ChatColor color = flag.getHeldBy().getColor();
            for (int i = 1; i <= 10; i++) {
                progressBar += (progress >= i ? color + Symbols.bigSquare : ChatColor.GRAY + Symbols.bigSquare); // squares 1 to 10
            }

            content.add(progressBar);
        }

        return content;

    }

    private List<String> getFlagProgressForSpec(Flag flag) {
        List<String> content = new ArrayList<String>();

        CTFTeam team = flag.getTeam();
        if (flag.is(FlagState.BASE)) {
            content.add(" " + team.getColor() + "§l" + Symbols.filledArrow + " §a" + getStringsApi().get("ctf.scoreboard.flag.secured").toString());
        } else {
            content.add(" " + team.getColor() + "§l" + Symbols.filledArrow + " §6" + getStringsApi().get("ctf.scoreboard.flag.stolen-by").toString() + "§l" + flag.getHeldBy().getColor() + flag.getHeldBy().getName());

            // Progress bar
            int progress = fm.getProgress(flag);
            String progressBar = " ";
            ChatColor color = flag.getHeldBy().getColor();
            for (int i = 0; i <= 10; i++) {
                progressBar += (progress >= i ? color + Symbols.bigSquare : ChatColor.GRAY + Symbols.bigSquare); // squares 1 to 10
            }

            content.add(progressBar);
        }

        return content;

    }

    /**
     * Build waiting inventory
     */
    @Override
    public Inventory getWaitingInventory() {

        Inventory waitInv = new Inventory(Constants.DEFAULT_WAITING_INV_NAME, "§aAttente", 1);
        Log.debug("Registering waiting inventory " + waitInv.getName());
        Material waitingTeamMaterial;
        try {
            waitingTeamMaterial = Material.valueOf(getApi().getConfig().getString("config.waiting-inventory.team-material", Constants.DEFAULT_WAIT_INVENTORY_CHOOSE_TEAM_MATERIAL));
        } catch (IllegalArgumentException ex) {
            waitingTeamMaterial = Material.valueOf(Constants.DEFAULT_WAIT_INVENTORY_CHOOSE_TEAM_MATERIAL);
        }

        // Choose team item
        if (getApi().getConfig().getBoolean("config.waiting-inventory.enable-choose-team", Constants.DEFAULT_ENABLE_CHOOSE_TEAM)) {
            Item chooseTeam = new Item.ItemBuilder(waitingTeamMaterial)
                .withPosition(0)
                .build();
            chooseTeam.setActions(
                Lists.newArrayList(
                    new OpenTeamInventoryBanner(getChooseTeamInventoryName())
                )
            );
            waitInv.addItem(chooseTeam);
        }

        // Book how to play
        Item howToPlay = new Item.ItemBuilder(Material.WRITTEN_BOOK)
            .withName(getStringsApi().get("messages.waiting-inventory.how-to-play").toString())
            .withPosition(1)
            .addBookPages(getStringsApi().getList("messages.waiting-inventory.help-pages").toStringList())
            .build();
        waitInv.addItem(howToPlay);

        // Fish leave game
        if (getApi().getConfig().getBoolean("config.bungee.enabled", Constants.DEFAULT_BUNGEE_ENABLED)) {
            Item leaveGame = new Item.ItemBuilder(Material.COD)
                .withDamage((short) 3)
                .withPosition(8)
                .withName(getStringsApi().get("messages.waiting-inventory.leave-game").toString())
                .build();
            leaveGame.setActions(
                Lists.newArrayList(
                    new BungeeTpAction(getApi().getConfig().getString("config.bungee.server", Constants.DEFAULT_LOBBY_SERVER))
                )
            );
            waitInv.addItem(leaveGame);
        }

        return waitInv;
    }

    @Override
    public HCTeam getForcedOnlineWinningTeam(List<HCTeam> onlineTeams) {
        CTFTeam winner = null;
        boolean isSameScore = false;
        for (HCTeam team : onlineTeams) {
            CTFTeam ctfTeam = (CTFTeam) team;
            if (ctfTeam.getFlagsTaken() > 0) {
                if (winner == null) {
                    winner = ctfTeam;
                } else {
                    if (ctfTeam.getFlagsTaken() == winner.getFlagsTaken()) {
                        isSameScore = true;
                    } else if (ctfTeam.getFlagsTaken() > winner.getFlagsTaken()) {
                        isSameScore = false;
                        winner = ctfTeam;
                    }
                }
            }
        }
        if (isSameScore) {
            return null;
        }

        return winner;
    }

    @Override
    public void printEndMessage(HCPlayer hcPlayer, HCTeam winningTeam) {

        if (hcPlayer.isOnline()) {

            CTFPlayer ctfPlayer = (CTFPlayer) hcPlayer;

            Texts.tellraw(hcPlayer.getPlayer(), getStringsApi().get("messages.end.winning-team")
                .replace("%game%", getApi().getName())
                .replace("%kills%", String.valueOf(hcPlayer.getKills()))
                .replace("%deaths%", String.valueOf(hcPlayer.getDeaths()))
                .replace("%hardcoins%", String.valueOf(hcPlayer.getMoney()))
                .replace("%flagstaken%", String.valueOf(ctfPlayer.getFlagsTaken()))
                .replace("%flagssaved%", String.valueOf(ctfPlayer.getFlagsSaved()))
                .replace("%timeplayed%", String.valueOf(hcPlayer.getTimePlayed()))
                .replace("%winner%", (winningTeam == null ? "Match nul" : winningTeam.getColor() + winningTeam.getName()))
                .toString()
            );

        }

    }

    private class OpenTeamInventoryBanner extends Action implements ItemModifier {

        private String inventoryName;

        public OpenTeamInventoryBanner(String inventoryName) {
            this.inventoryName = inventoryName;
        }

        @Override
        public void executeAction(Player player, SigPlayer sigPlayer) {
            Inventory inv = InventoryManager.instance().getInventoryByName(inventoryName);
            if (inv != null) {
                inv.openInventory(player, sigPlayer);
            }
            executeNextAction(player, sigPlayer, true);
        }

        @Override
        public Item getAlternateVersion(Player player, SigPlayer sigPlayer) {
            HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
            return new Item.ItemBuilder(ItemUtils.toBannerMaterial(hcPlayer.getColor()))
                .withName(getStringsApi().get("messages.waiting-inventory.choose-team").toString())
                .build();
        }

        @Override
        public boolean isModifier() {
            return true;
        }

    }
}
