package com.gmail.val59000mc.ctf.flags;

import com.gmail.val59000mc.ctf.common.CTFItems;
import com.gmail.val59000mc.ctf.events.CTFFlagReturnedToBaseEvent;
import com.gmail.val59000mc.ctf.players.CTFPlayer;
import com.gmail.val59000mc.ctf.players.CTFTeam;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.tasks.HCTaskScheduler;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.gmail.val59000mc.spigotutils.items.PotionMetaBuilder;
import com.google.common.collect.Sets;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionType;

import java.util.Calendar;
import java.util.List;

public class FlagManager {

    private Flag redFlag;
    private Flag blueFlag;
    private double maxDistanceBetweenFlags;
    private HCGameAPI api;
    private HCTaskScheduler updateTask;

    public FlagManager(HCGameAPI api) {
        this.redFlag = null;
        this.blueFlag = null;
        this.api = api;
        this.maxDistanceBetweenFlags = Double.MAX_VALUE;
        this.updateTask = null;
    }


    public Flag getRedFlag() {
        return redFlag;
    }

    public Flag getBlueFlag() {
        return blueFlag;
    }

    public void createFlags() {
        Log.debug("Create flags");

        World world = api.getWorldConfig().getWorld();
        FileConfiguration cfg = api.getConfig();

        Location redLoc = Parser.parseLocation(world, cfg.getString("teams.red.flag"));
        BlockFace redFacing = BlockFace.valueOf(cfg.getString("teams.red.flag-facing", "SOUTH"));
        CTFTeam redTeam = (CTFTeam) api.getPlayersManagerAPI().getHCTeam("Rouge");
        redFlag = createBanner(redLoc, DyeColor.RED, redTeam, redFacing);
        Log.debug("Red flag create at " + Locations.printLocation(redFlag.getLoc()));
        redTeam.setFlag(redFlag);

        Location blueLoc = Parser.parseLocation(world, cfg.getString("teams.blue.flag"));
        BlockFace blueFacing = BlockFace.valueOf(cfg.getString("teams.blue.flag-facing", "NORTH"));
        CTFTeam blueTeam = (CTFTeam) api.getPlayersManagerAPI().getHCTeam("Bleu");
        blueFlag = createBanner(blueLoc, DyeColor.BLUE, blueTeam, blueFacing);
        Log.debug("Blue flag create at " + Locations.printLocation(blueFlag.getLoc()));
        blueTeam.setFlag(blueFlag);


        maxDistanceBetweenFlags = redLoc.distance(blueLoc);

    }

    private void removeFlags() {
        redFlag.removeBaseBanner();
        blueFlag.removeBaseBanner();
    }

    private Flag createBanner(Location loc, DyeColor color, CTFTeam team, BlockFace facing) {
        Flag flag = new Flag(api, team, loc, color, facing);
        flag.createBaseBanner();
        return flag;
    }

    public Flag getFlagOn(Location location) {
        if (redFlag.isFlagOn(location)) {
            return redFlag;
        }

        if (blueFlag.isFlagOn(location)) {
            return blueFlag;
        }

        return null;
    }

    public void takeFlag(CTFPlayer ctfPlayer, Flag flag) {
        flag.removeBaseBanner();
        ctfPlayer.setCarriedFlag(flag);
        flag.setHeldBy(ctfPlayer);
        flag.setState(FlagState.MOVING);
        ctfPlayer.getPlayer().getInventory().setHelmet(CTFItems.getBanner(flag.getTeam()));

        // local variables
        CTFTeam thiefTeam = (CTFTeam) ctfPlayer.getTeam();
        CTFTeam enemyTeam = thiefTeam.getEnemyTeam();
        String thiefName = ctfPlayer.getColor() + ctfPlayer.getName();
        String enemyTeamName = enemyTeam.getColor() + enemyTeam.getName();

        // message thief's team
        api.getStringsAPI()
            .get("ctf.enemy-flag-taken-by")
            .replace("%player%", thiefName) // has stolen
            .sendChatP(thiefTeam)
            .sendActionBar(thiefTeam);

        // sound theif's team
        api.getSoundAPI().play(thiefTeam, Sound.ENTITY_PLAYER_LEVELUP, 1, 2);

        // message flag's team
        api.getStringsAPI()
            .get("ctf.ally-flag-taken-by")
            .replace("%player%", thiefName) // has stolen
            .sendChatP(enemyTeam)
            .sendActionBar(enemyTeam);

        // sound flag's team
        api.buildTask("alert sound : ally flag taken", new HCTask() {

            @Override
            public void run() {
                api.getSoundAPI().play(enemyTeam, Sound.BLOCK_NOTE_BLOCK_PLING, 1, 2);
            }
        })
            .withInterval(5)
            .withIterations(5)
            .build()
            .start();


        // message specs
        List<HCPlayer> specs = api.getPlayersManagerAPI().getPlayersMultiplesStates(true, Sets.newHashSet(PlayerState.DEAD, PlayerState.SPECTATING, PlayerState.VANISHED));
        api.getStringsAPI()
            .get("ctf.flag-taken-by")
            .replace("%player%", thiefName) // has stolen
            .replace("%team%", enemyTeamName) // flag's team
            .sendChatP(specs)
            .sendActionBar(specs);

        // sound spec
        api.getSoundAPI().play(specs, Sound.ENTITY_WITHER_HURT, 1, 2);

        // spawn firework
        Location location = flag.getCurrentLocation().clone().add(0, 2, 0);
        detonateFireworkAtLocation(location, CTFItems.getBukkitColor(flag.getTeam()));

        startFlagProgress(flag);
    }


    public void dropFlag(CTFPlayer ctfPlayer) {
        Flag flag = ctfPlayer.getCarriedFlag();
        flag.createBaseBanner();
        ctfPlayer.setCarriedFlag(null);
        ctfPlayer.getPlayer().getInventory().remove(Material.WHITE_BANNER);
        flag.setHeldBy(null);
        flag.setState(FlagState.BASE);
        ctfPlayer.getPlayer().getInventory().setHelmet(CTFItems.getHelmet((CTFTeam) ctfPlayer.getTeam()));

        // local variables
        CTFTeam thiefTeam = (CTFTeam) ctfPlayer.getTeam();
        CTFTeam enemyTeam = thiefTeam.getEnemyTeam();
        String thiefName = ctfPlayer.getColor() + ctfPlayer.getName();
        String enemyTeamName = enemyTeam.getColor() + enemyTeam.getName();

        // message thief's team
        api.getStringsAPI()
            .get("ctf.enemy-flag-dropped-by")
            .replace("%player%", thiefName) // has stolen
            .sendChatP(thiefTeam)
            .sendActionBar(thiefTeam);

        // sound thief's team
        api.getSoundAPI().play(thiefTeam, Sound.ENTITY_VILLAGER_DEATH, 1, 1);

        // message flag's team
        api.getStringsAPI()
            .get("ctf.ally-flag-dropped-by")
            .replace("%player%", thiefName) // has stolen
            .sendChatP(enemyTeam)
            .sendActionBar(enemyTeam);

        // sound flag's team
        api.getSoundAPI().play(enemyTeam, Sound.ENTITY_VILLAGER_DEATH, 1, 1);


        // message specs
        List<HCPlayer> specs = api.getPlayersManagerAPI().getPlayersMultiplesStates(true, Sets.newHashSet(PlayerState.DEAD, PlayerState.SPECTATING, PlayerState.VANISHED));
        api.getStringsAPI()
            .get("ctf.flag-dropped-by")
            .replace("%player%", thiefName) // has stolen
            .replace("%team%", enemyTeamName) // flag's team
            .sendChatP(specs)
            .sendActionBar(specs);

        // sound spec
        api.getSoundAPI().play(specs, Sound.ENTITY_WITHER_HURT, 1, 2);

        Bukkit.getPluginManager().callEvent(new CTFFlagReturnedToBaseEvent(api, flag));
    }

    public void dropFlagBy(CTFPlayer ctfKilled, CTFPlayer ctfKiller) {
        dropFlag(ctfKilled);
        ctfKiller.addFlagsSaved();
        api.getStringsAPI().get("ctf.killed-enemy-carrying-flag").sendChatP(ctfKiller);
        api.getPlayersManagerAPI().rewardMoneyTo(ctfKiller, api.getConfig().getDouble("rewards.kill-enemy-carrying-ally-flag", 3));
    }

    public void scoreFlag(CTFPlayer ctfPlayer) {

        if (((CTFTeam) ctfPlayer.getTeam()).getFlag().is(FlagState.MOVING)) {
            warnFlagNotAtBase(ctfPlayer);
            return;
        }

        Flag flag = ctfPlayer.getCarriedFlag();
        flag.createBaseBanner();
        ctfPlayer.setCarriedFlag(null);
        flag.setHeldBy(null);
        flag.setState(FlagState.BASE);
        ctfPlayer.getPlayer().getInventory().setHelmet(CTFItems.getHelmet((CTFTeam) ctfPlayer.getTeam()));

        ctfPlayer.addFlagsTaken();
        api.getPlayersManagerAPI().rewardMoneyTo(ctfPlayer, api.getConfig().getDouble("rewards.capture-enemy-flag", 5));

        // local variables
        String thiefName = ctfPlayer.getColor() + ctfPlayer.getName();
        String thiefTeamName = ctfPlayer.getTeam().getColor() + ctfPlayer.getTeam().getName();
        CTFTeam thiefTeam = (CTFTeam) ctfPlayer.getTeam();
        CTFTeam enemyTeam = thiefTeam.getEnemyTeam();

        // message thief's team
        api.getStringsAPI()
            .get("ctf.enemy-flag-captured-by")
            .replace("%player%", thiefName) // has stolen
            .sendChatP(thiefTeam)
            .sendActionBar(thiefTeam);

        // sound thief's team
        api.getSoundAPI().play(thiefTeam, Sound.ENTITY_ENDER_DRAGON_GROWL, 1, 2);

        // message enemy's team
        api.getStringsAPI()
            .get("ctf.ally-flag-captured-by")
            .replace("%player%", thiefName) // has stolen
            .sendChatP(enemyTeam)
            .sendActionBar(enemyTeam);

        // sound enemy's team
        api.getSoundAPI().play(enemyTeam, Sound.ENTITY_WITHER_SPAWN, 1, 1);

        // message specs
        List<HCPlayer> specs = api.getPlayersManagerAPI().getPlayersMultiplesStates(true, Sets.newHashSet(PlayerState.DEAD, PlayerState.SPECTATING, PlayerState.VANISHED));
        api.getStringsAPI()
            .get("ctf.flag-captured-by")
            .replace("%player%", thiefName) // has stolen
            .replace("%team%", thiefTeamName) // thief's team
            .sendChatP(specs)
            .sendActionBar(specs);

        // sound spec
        api.getSoundAPI().play(specs, Sound.ENTITY_WITHER_HURT, 1, 2);

        // title all
        api.getStringsAPI()
            .get("ctf.team-score-increased")
            .replace("%team%", thiefTeamName) // scored team
            .sendTitle(10, 30, 10);


        // spawn firework
        Location location = flag.getCurrentLocation().clone().add(0, 2, 0);
        detonateFireworkAtLocation(location, CTFItems.getBukkitColor(thiefTeam));

        Bukkit.getPluginManager().callEvent(new CTFFlagReturnedToBaseEvent(api, flag));

        checkIfTeamHasWon(thiefTeam);
    }


    private void warnFlagNotAtBase(CTFPlayer ctfPlayer) {
        long now = Calendar.getInstance().getTimeInMillis();
        if (now - ctfPlayer.getLastWarnNotAtBase() > 5000) {
            ctfPlayer.setLastWarnNotAtBase(now);
            api.getSoundAPI()
                .play(ctfPlayer, Sound.ENTITY_VILLAGER_NO, 1, 2f);
            api.getStringsAPI()
                .get("ctf.cannot-score-while-flag-moves")
                .sendChatP(ctfPlayer);
        }
    }


    private void checkIfTeamHasWon(CTFTeam thiefTeam) {
        if (thiefTeam.getFlagsTaken() >= api.getConfig().getInt("flags-limit", 3)) {
            removeFlags();
            api.endGame(thiefTeam);
        }
    }


    private void detonateFireworkAtLocation(Location location, Color bukkitColor) {
        Firework fw = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();
        fwm.setPower(0);
        fwm.addEffect(FireworkEffect.builder()
            .flicker(true)
            .withColor(bukkitColor)
            .withFade(Color.fromRGB(255, 255, 255))
            .with(Type.BALL_LARGE)
            .trail(true)
            .build()
        );
        fw.setFireworkMeta(fwm);
        Bukkit.getScheduler().runTaskLater(api.getPlugin(), new Runnable() {
            public void run() {
                fw.detonate();
            }
        }, 2);
    }

    private void startFlagProgress(Flag flag) {

        if (updateTask == null || !updateTask.isRunning()) {

            updateTask = api.buildTask("update scoreboard while flag is gone", new HCTask() {

                private int iterations = 0;

                @Override
                public void run() {
                    iterations++;
                    api.getPlayersManagerAPI().updatePlayersScoreboards();
                    doEventsProgress(redFlag);
                    doEventsProgress(blueFlag);
                }

                private void doEventsProgress(Flag flag) {
                    if (flag.is(FlagState.MOVING)) {

                        Location location = flag.getCurrentLocation();

                        if (iterations % 6 == 4) {

                            // Launch firework
                            Firework fw = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
                            FireworkMeta fwm = fw.getFireworkMeta();
                            fwm.setPower(1);
                            fwm.addEffect(FireworkEffect.builder()
                                .flicker(true)
                                .withColor(CTFItems.getBukkitColor((CTFTeam) flag.getTeam()))
                                .withFade(Color.fromRGB(255, 255, 255))
                                .with(Type.STAR)
                                .trail(true)
                                .build()
                            );
                            fw.setFireworkMeta(fwm);

                        } else if (iterations % 5 == 1) {

                            // Heal player with potion
                            ThrownPotion heal = (ThrownPotion) location.getWorld().spawnEntity(location, EntityType.SPLASH_POTION);
                            heal.setVelocity(flag.getHeldBy().getPlayer().getVelocity());
                            heal.setItem(new ItemBuilder(Material.SPLASH_POTION)
                                .buildMeta(PotionMetaBuilder.class)
                                .withMainEffect(PotionType.INSTANT_HEAL)
                                .item()
                                .build());

                        }


                    }
                }

            }).addListener(new HCTaskListener() {

                @EventHandler
                public void whenFlagHasReturned(CTFFlagReturnedToBaseEvent e) {

                    if (e.getFlag().equals(flag)) {
                        if (flag.getTeam().getEnemyTeam().getFlag().is(FlagState.BASE)) { // stop task only if other flag is not moving
                            getScheduler().stop();
                            getPmApi().updatePlayersScoreboards();
                        }
                    }
                }

            })
                .withInterval(20)
                .build();

            updateTask.start();

        }

    }

    public int getProgress(Flag flag) {
        double dist = flag.getCurrentLocation().distance(flag.getTeam().getEnemyTeam().getFlag().getLoc());
        dist = Math.min(dist, maxDistanceBetweenFlags);
        return 10 - (int) Math.floor(dist / maxDistanceBetweenFlags * 10);
    }
}
