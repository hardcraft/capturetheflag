package com.gmail.val59000mc.ctf.flags;

import com.gmail.val59000mc.ctf.players.CTFPlayer;
import com.gmail.val59000mc.ctf.players.CTFTeam;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.block.data.Rotatable;

public class Flag {

    private CTFTeam belongsToTeam;
    private FlagState state;
    private CTFPlayer heldBy;
    private HCGameAPI api;
    private Location loc;
    private DyeColor color;
    private BlockFace facing;

    public Flag(CTFTeam team, Banner banner) {
        this.belongsToTeam = team;
        this.state = FlagState.BASE;
        this.heldBy = null;
    }

    public Flag(HCGameAPI api, CTFTeam team, Location loc, DyeColor color, BlockFace facing) {
        this.belongsToTeam = team;
        this.loc = loc;
        this.color = color;
        this.facing = facing;
        this.state = FlagState.BASE;
        this.heldBy = null;
        this.api = api;
    }

    public void createBaseBanner() {
        Block block = api.getWorldConfig().getWorld().getBlockAt(loc);
        block.setType(Material.WHITE_BANNER);
        Banner banner = (Banner) block.getState();

        // set direction
        Rotatable bannerData = (Rotatable) banner.getBlockData();
        bannerData.setRotation(facing);
        banner.setBlockData(bannerData);

        // set colors
        banner.setBaseColor(color);
        banner.addPattern(new Pattern(DyeColor.BLACK, PatternType.GRADIENT_UP));
        banner.addPattern(new Pattern(color, PatternType.TRIANGLES_TOP));
        banner.addPattern(new Pattern(color, PatternType.TRIANGLES_BOTTOM));
        banner.addPattern(new Pattern(color, PatternType.CURLY_BORDER));
        banner.addPattern(new Pattern(color, PatternType.BORDER));
        banner.addPattern(new Pattern(color, PatternType.STRIPE_SMALL));

        banner.update(true);
    }

    public void removeBaseBanner() {
        Block block = api.getWorldConfig().getWorld().getBlockAt(loc);
        block.setType(Material.AIR);
    }

    public Location getCurrentLocation() {
        if (is(FlagState.BASE)) {
            return loc;
        } else {
            return heldBy.getPlayer().getLocation();
        }
    }

    public Location getLoc() {
        return loc;
    }

    public CTFPlayer getHeldBy() {
        return heldBy;
    }

    public void setHeldBy(CTFPlayer heldBy) {
        this.heldBy = heldBy;
    }

    public CTFTeam getTeam() {
        return belongsToTeam;
    }

    public boolean is(FlagState state) {
        return this.state.equals(state);
    }

    public FlagState getState() {
        return state;
    }

    public void setState(FlagState state) {
        this.state = state;
    }

    public boolean isFlagOn(Location location) {
        return loc.getWorld().equals(location.getWorld()) && loc.distanceSquared(location) < 1;
    }

}
