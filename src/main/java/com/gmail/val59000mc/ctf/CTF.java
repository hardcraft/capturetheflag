package com.gmail.val59000mc.ctf;

import com.gmail.val59000mc.ctf.callbacks.CTFCallbacks;
import com.gmail.val59000mc.ctf.listeners.CTFBlocksListener;
import com.gmail.val59000mc.ctf.listeners.CTFDamageListener;
import com.gmail.val59000mc.ctf.listeners.CTFGamePlayListener;
import com.gmail.val59000mc.ctf.listeners.CTFMySQLListener;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.spigotutils.Configurations;
import com.google.common.collect.Sets;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class CTF extends JavaPlugin {

    public void onEnable() {

        this.getDataFolder().mkdirs();
        File config = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "config.yml"), new File(this.getDataFolder(), "config.yml"));
        File lang = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "lang.yml"), new File(this.getDataFolder(), "lang.yml"));

        HCGameAPI game = new HCGame.Builder("Capture The Flag", this, config, lang)
            .withPluginCallbacks(new CTFCallbacks())
            .withDefaultListenersAnd(Sets.newHashSet(
                new CTFGamePlayListener(),
                new CTFBlocksListener(),
                new CTFMySQLListener(),
                new CTFDamageListener()
            ))
            .build();
        game.loadGame();
    }

    public void onDisable() {
    }
}
