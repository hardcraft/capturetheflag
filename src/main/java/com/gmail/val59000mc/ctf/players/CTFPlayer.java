package com.gmail.val59000mc.ctf.players;

import com.gmail.val59000mc.ctf.flags.Flag;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import org.bukkit.entity.Player;

public class CTFPlayer extends HCPlayer {

    private int flagsTaken;
    private int flagsSaved;
    private Flag carriedFlag;
    private double tntPower;
    private double tntProtection;
    private int tntAmount;
    private long lastWarnNotAtBase;

    public CTFPlayer(Player player) {
        super(player);
        this.flagsTaken = 0;
        this.flagsSaved = 0;
        this.carriedFlag = null;

        this.tntAmount = 1;
        this.tntPower = 20;
        this.tntProtection = 0;
        this.lastWarnNotAtBase = 0;
    }

    public CTFPlayer(HCPlayer hcPlayer) {
        super(hcPlayer);
        CTFPlayer ctfPlayer = (CTFPlayer) hcPlayer;
        this.flagsTaken = ctfPlayer.flagsTaken;
        this.flagsSaved = ctfPlayer.flagsSaved;
        this.carriedFlag = ctfPlayer.carriedFlag;
        this.tntPower = ctfPlayer.tntPower;
        this.tntProtection = ctfPlayer.tntProtection;
        this.tntAmount = ctfPlayer.tntAmount;
        this.lastWarnNotAtBase = ctfPlayer.lastWarnNotAtBase;
    }

    public void subtractBy(HCPlayer lastUpdatedSession) {
        super.subtractBy(lastUpdatedSession);
        CTFPlayer ctfPlayer = (CTFPlayer) lastUpdatedSession;
        this.flagsTaken -= ctfPlayer.flagsTaken;
        this.flagsSaved -= ctfPlayer.flagsSaved;
    }


    public long getLastWarnNotAtBase() {
        return lastWarnNotAtBase;
    }

    public void setLastWarnNotAtBase(long lastWarnNotAtBase) {
        this.lastWarnNotAtBase = lastWarnNotAtBase;
    }

    public double getTntPower() {
        return tntPower;
    }

    public void setTntPower(double tntPower) {
        this.tntPower = tntPower;
    }

    public double getTntProtection() {
        return tntProtection;
    }

    public void setTntProtection(double tntProtection) {
        this.tntProtection = tntProtection;
    }

    public int getTntAmount() {
        return tntAmount;
    }

    public void setTntAmount(int tntAmount) {
        this.tntAmount = tntAmount;
    }


    public Flag getCarriedFlag() {
        return carriedFlag;
    }

    public void setCarriedFlag(Flag carriedFlag) {
        this.carriedFlag = carriedFlag;
    }

    public boolean isCarryingFlag() {
        return carriedFlag != null;
    }

    public int getFlagsTaken() {
        return flagsTaken;
    }

    public int getFlagsSaved() {
        return flagsSaved;
    }

    public void addFlagsTaken() {
        this.flagsTaken++;
        if (hasTeam()) {
            ((CTFTeam) getTeam()).addFlagTaken();
        }
    }

    public void addFlagsSaved() {
        this.flagsSaved++;
    }

}
