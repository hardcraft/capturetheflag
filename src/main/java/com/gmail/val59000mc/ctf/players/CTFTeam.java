package com.gmail.val59000mc.ctf.players;

import com.gmail.val59000mc.ctf.flags.Flag;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.spigotutils.Randoms;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.util.List;

public class CTFTeam extends HCTeam {

    private List<Location> spawnpoints;
    private int flagsTaken;
    private CTFTeam enemyTeam;
    private Flag flag;


    public CTFTeam(String name, ChatColor color, List<Location> spawnpoints) {
        super(name, color, null);
        this.spawnpoints = spawnpoints;
        this.enemyTeam = null;
        this.flag = null;
        this.flagsTaken = 0;
    }

    public Flag getFlag() {
        return flag;
    }

    public void setFlag(Flag flag) {
        this.flag = flag;
    }

    public List<Location> getSpawnpoints() {
        return spawnpoints;
    }

    public Location getRandomSpawnpoint() {
        return spawnpoints.get(Randoms.randomInteger(0, spawnpoints.size() - 1)).clone();
    }

    public int getFlagsTaken() {
        return flagsTaken;
    }

    public void addFlagTaken() {
        this.flagsTaken++;
    }

    public CTFTeam getEnemyTeam() {
        return enemyTeam;
    }

    public void setEnemyTeam(CTFTeam enemyTeam) {
        this.enemyTeam = enemyTeam;
    }

}
